/*
*
*     /\  \          ___        /\  \
*    /::\  \        /\  \      /::\  \
*   /:/\:\  \       \:\  \    /:/\:\  \
*  /::\~\:\__\      /::\__\  /:/  \:\  \
* /:/\:\ \:|__|  __/:/\/__/ /:/__/_\:\__\
* \:\~\:\/:/  / /\/:/  /    \:\  /\ \/__/
*  \:\ \::/  /  \::/__/      \:\ \:\__\
*   \:\/:/  /    \:\__\       \:\/:/  /
*    \::/__/      \/__/        \::/  /
*     ~~                        \/__/
*      ___           ___                   ___           ___
*     /\  \         /\__\      ___        /\  \         /\  \
*    /::\  \       /:/  /     /\  \      /::\  \       /::\  \
*   /:/\ \  \     /:/  /      \:\  \    /:/\:\  \     /:/\:\  \
*  _\:\~\ \  \   /:/  /       /::\__\  /:/  \:\__\   /::\~\:\  \
* /\ \:\ \ \__\ /:/__/     __/:/\/__/ /:/__/ \:|__| /:/\:\ \:\__\
* \:\ \:\ \/__/ \:\  \    /\/:/  /    \:\  \ /:/  / \:\~\:\ \/__/
*  \:\ \:\__\    \:\  \   \::/__/      \:\  /:/  /   \:\ \:\__\
*   \:\/:/  /     \:\  \   \:\__\       \:\/:/  /     \:\ \/__/
*    \::/  /       \:\__\   \/__/        \::/__/       \:\__\
*     \/__/         \/__/                 ~~            \/__/
*
* A tiny jQuery plugin for slide panel navigation
* Created by Adam D. Scott (www.adamdscott.com)
* You may use bigSlide.js under the terms of the MIT License.
*/

(function($) {
  'use strict';

  $.fn.bigSlide = function(options) {

    var settings = $.extend({
      'menu': ('#menu'),
      'container': ('#content-wrap'),
      'push': ('.push'),
      'header': ('#header'),
      'menuWidth': '20%',
      'speed': '300'
    }, options);

    var menuLink = this,
        menu = $(settings.menu),
        container = $(settings.container),
        push = $(settings.push),
        header = $(settings.header),
        width = settings.menuWidth;

    var positionOffScreen = {
      'position': 'fixed',
      'top': '0',
      'bottom': '0',
      'left': '-' + settings.menuWidth,
      'width': settings.menuWidth,
      'height': '100%'
    };

    var animateSlide = {
      '-webkit-transition': 'left ' + settings.speed + 'ms ease',
      '-moz-transition': 'left ' + settings.speed + 'ms ease',
      '-ms-transition': 'left ' + settings.speed + 'ms ease',
      '-o-transition': 'left ' + settings.speed + 'ms ease',
      'transition': 'left ' + settings.speed + 'ms ease'
    };


    menu.css(positionOffScreen);
    menu.css(animateSlide);
    push.css(animateSlide);
    header.css(animateSlide);

    menu._state = 'closed';

    menu.open = function() {
      menu._state = 'open';
      menu.css('left', '0');
      push.css('left', width);
      header.css('left', width);
      container.css('width', '80%');
    };

    menu.close = function() {
      menu._state = 'closed';
      menu.css('left', '-' + width);
      push.css('left', '0');
      header.css('left', '0');
      container.css('width', '100%');

    };

    menuLink.on('click.bigSlide', function(e) {
      e.preventDefault();
      if (menu._state === 'closed') {
        menu.open();
      } else {
        menu.close();
      }
    });

//Trigger menu above if window width > 1200px
function checkWidth() {
        var windowSize = $(window).width();

        if (windowSize <= 1199 && menu._state === 'open') {
            menu.close();
        }

        else if (windowSize >= 1200 && menu._state === 'closed') {
            menu.open();
        }
    }

    // Execute on load
    checkWidth();
    // Bind event listener
    $(window).resize(checkWidth);

  };

    return menu;



}(jQuery));
